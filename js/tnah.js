/*

RIGHT THEN HENRY!....THIS  IS THE MAIN SCRIPT THAT CONTROLS THE INTERACTIONS AND THE "GAME ENGINE"
THERE'S QUITE A BIT WE HAVE TO THING ABOUT, SO THIS IS WHERE WE CAN TRY STUFF OUT, BUILD THE BASIC
BLOCKS WE NEED ETC....BELOW IS A LIST OF THE STUFF WE NEED AND A LIST OF THE STUFF WE ALREADY HAVE..



================================== STUFF WE HAVE ALREADY ==================================================

1. CATEGORY: GAME ENGINE - WEB BROWSER BASED - THIS IS IMPORTANT AND WILL ALLOW US TO DELIVER OUR GAME TO MULITPLE PLATFORMS & DEVICES...ITS ALL JUST HTML5..

2. CATEGORY: GAME ENGINE - COMMON LOOK AND FEEL THAT CAN BE TWEAKED AND CHANGED AS NEEDED USING CSS - THIS WILL MAKE IT EASY TO CHANGE THE COLORS, FONTS, IMAGES ETC. USEFUL INDEED!

3. CATEGORY: GAME ENGINE - RESPONSIVE SIZES - ONLY THE BASICS IN PLACE SO FAR, AND THIS WILL NEED MORE EFFORT AS WE PROGRESS, BUT IF YOU RESIZE YOUR BROWSER WINDOW, OR TRY THIS ON A DIFFERENT DEVICE, YOU SHOULD SEE IT RESIZE/SCALE TO FIT THE DEVICE SCREEN. AGAIN, THIS IS VERY IMPORTANT IF WE WANT TO ALLOW PLAYERS ON DIFFERENT DEVICES, SCREENSIZES AND OPERATING SYSTEMS!

4. CATEGORY: GAME ENGINE - MENU SYSTEM. THIS USES HTML, JAVASCRIPT AND CSS. WE CAN ADD, TWEAK AND REMOVE MENU OPTIONS, AND THE MENU CAN CONTROL THE USER INTERFACE. THIS IS A FUNDAMENTAL, OBVIOUSLY!

5. CATEGORY: GAME ENGINE - TABBED PANELS. THIS USES HTML, CSS AND JAVASCRIPT, AND WE CAN EASILY ADD/REMOVE/EDIT TABS AND THEIR CONTENT. ANOTHER FUNDAMENTAL FOR OUR ENGINE

6. CATEGORY: LIGHTS - ABILITY TO TURN LIGHTS ON AND OFF, BOTH BY USER INTERACTION, OR BY CODE (GAME EVENTS, RANDOM SCARES ETC).

7. CATEGORY: LIGHTS - ABILITY TO KEEP SET A MAXIMUM NUMBER OF TIMES THE USER CAN TURN THE LIGHT ON

8. CATEGORY: LIGHTS - ABILITY TO KNOW IF THE LIGHT IS ON OR OFF

9. CATEGORY: LIGHTS - ABILITY TO KEEP TRACK OF HOW MANY TIMES THE USER HAS TURNED THE LIGHT ON OR OFF

10. CATEGORY: LIGHTS - ABILITY TO UPDATE THE USER INTERFACE WITH LIGHT TOGGLE RELATED DATA (HOW MANY ALLOWED, HOW MANY USED, LIGHT ON, LIGHT OFF ETC)

11. CATEGORY: JUMPSCARE - ABILLTY TO SHOW SCARE GRAPHIC - AT PRESENT, THIS IS HARD-CODED TO SHOW ONLY A SINGLE GRAPHIC, BUT WE CAN EASILY ADD MORE, AND MAKE IT SHOW RANDOM SCRAE IMAGES OR VIDEOS OR ANIMATIONS :-) 

12. CATEGORY: SOUNDS - ABILITY TO PLAY SOUNDS

13. CATEGORY: JUMPSCARE - ABILITY TO PLAY A SCARE SOUND ON DEMAND (AT PRESENT IT FIRES WHEN THE USER TRIES TO EXCEED THEIR LIGHT ON/OFF TOGGLE LIMIT). WE CAN HAVE A RANGE OR SOUNDS SO IT CHANGES EACH TIME IT IS PLAYED MAYBE?

14. CATEGORY: GAME ENGINE - DELAYED EVENTS. SOMETIMES WE WILL WANT TO DO SOMETHING, THEN MAKE SOMETHING ELSE HAPPEN SORTLY AFTERWARDS. TO SEE THIS IN ACTION, FIRE THE JUMPSCARE. THERE IS A DELAY AFTER IT FIRES, THEN THE MMWWWAHAHAAAHAAH GOTCHA MESSAGE SHOWS WITH THE RESET BUTTON..

15. CATEGORY: USER EXPERIENCE - GRAMMATICALLY CORRECT COUNTING OUTPUT - EG: WHEN YOU TOGGLE THE LIGHT IN THE DEV TOOLS PAGE, THE LIGHT USAGE PANEL SHOW SOMETHING LIKE "YOU HAVE'NT TOGGLED THE LIGHT YET" OR "YOU HAVE TOGGLED THE LIGHT TWICE", OR FINALLY, "YOU HAVE TOGGLED THE LIGHT (number) TIMES". THIS ISNT REALLY VITAL TO OUR GAME, BUT, ITS A NICE TOUCH, ATTENTION TO DETAIL, AND BETTER THAN ALWAYS JUST SHOWING "YOU HAVE TOGGLED THE LIGHT (number) TIME(S) - ASK ME ABOUT THIS IF YOU'RE NOT SURE WHAT I MEAN HERE.

16. CATEGORY: DEPLOYMENT. - CODEPEN IS GOOD FOR TRYING STUFF OUT, BUT WE NEED A PROPER SERVER SETUP FOR THE GAME ENGINE - See http://tnahsnowmeister.co.uk :-)

17. CATEGORY: PROCESS - WE NEED TO MAKE SURE WE PRODUCE GOOD CODE, AND TO PROTECT IT AGAINST ACCIDENTS SUCH AS CHANGING CODE AND BREAKING THE ENTIRE GAME ETC! WE NOW HAVE A "GIT REPOSITORY" - THIS WILL KEEP OUR CODE SAFE! - See https://bitbucket.org/snowmeister/tnah

18. CATEGORY: DOCUMENTATION - ANY CODE GETS HARD TO MANAGE AS IT INCREASES IN COMPLEXITY, AND OUR GAME ENGINE IS NO EXCEPTION. WE MUST ENSURE WE KEEP NOTES ABOUT HOW THINGS WORK SO WE CAN REFER BACK TO THEM LATER, AND TO HELP US BUILD OUR ENGINE QUICKLY! THIS IS FUNDAMENTAL DOOD!! WE NOW HAVE A WIKI, WHICH SHOULD BE KEPT UP TO DATE WITH IDEAS, HOWTOS ETC


================================== STUFF WE STILL NEED ==================================================

1. CATEGORY: GAME ENGINE - CHANGE SCREEN BACKGROUND. AT THE MOMENT, WE HAVE ONE BACKGROUND IMAGE. WE SHOULD BE ABLE TO LOAD AND SHOW OTHERS ON DEMAND, EITHER BY USER INTERACTION (WHEN BUTTONS ARE CLICKED ETC), OR FROM CODE, BASED ON EVENTS FIRED FROM THE GAME ENGINE

2. CATEGORY: GAME ENGINE - MULITPLE BACKGROUNDS DISPLAYED AT ONCE. THIS ISNT VITAL RIGHT NOW, BUT IF IMPLEMENTED WOULD ALLOW US TO USE "PARRALAX" EFFECTS, WHICH WOULD GIVE AN ADDED VISUAL "DEPTH". IF YOU WANT TO KNOW MORE, HAVE A LOOK AT http://scrollmagic.io/examples/advanced/parallax_scrolling.html OR  http://haxeflixel.com/demos/Parallax/ OR TO SEE IT IN EFFECTIVE 2D TOP-DOWN USE, CHECK OUT http://www.dangersoffracking.com/

3. CATEGORY: DATA - WE NEED THE ABILITY TO KEEP TRACK OF WHO IS WHO. WITHOUT THIS THERE IS NO LEAGUE TABLE!

4. CATEGORY: DATA - WE NEED THE ABILITY TO KEEP TRACK OF WHO IS WHO. WITHOUT THIS THERE IS NO LEAGUE TABLE!

5. CATEGORY: BACKEND - WE NEED TO DECIDE WHICH BACKEND LANGUAGE AND DATABASE TO USE - PHP/MYSQL, OR NODEJS/MONGO

*/


// ----------------------------------- VARIABLES ------------------------------------------
// For now, only a FEW variables....

// How much charge we lose PER SECOND of light usage...default value is 1, in other words
// we can have the light on for 100 seconds...
var numChargePercentageLostPerSecond = 1;

// Not entirely sure if this is the right approach, but gives aome interesting scope for score and light usage limits :-)
var numTimesLightUsed = 0;

// Again, not sure if this is useful or not yet, but
// let's set a limit to HOW MANY TIMES the user can
// switch the light on or off! For now, while we learn,
// I've set it to 20. Time to experiment to work out the best amount :-)
//
var numMaxLightUses = Math.floor(Math.random() * 8) + 5;

// We need to keep track of how many REMAINING light toggles we have left
var numRemainingLightUses = numMaxLightUses;

// Is the light ON or OFF - This variable will allow us to keep track!
var boolLightOn = true;

// ------------------- DEV variables. ONLY required while we are working on the prototype here! --------

var firstRun = true;

// ----------------------------------- END of VARIABLES stuff ------------------------------------------

// --------------------  REUSABLE FUNCTIONS - Think of these as BRICKS... -------------------
// This function takes a MENU ITEM NAME, eg: "options", "new", "help" etc, and then decides WHICH SCREEN TO SHOW
function fireMenu(eventName) {
    if(eventName === 'torch'){
        window.location = 'torch.html';
        return false;
    }
    $('#menu').fadeOut('slow', function() {
        $('#' + eventName).removeClass('hidden').fadeIn('fast');
    });
}

// This function just fires...and puts everything back to the beginning..
function resetUI() {
    $('div.screen').each(function() {
        if (!$(this).hasClass('hidden')) {
            $(this).addClass('hidden');
        }
    });
    $('#text-light-status').text('ON');
    $('#toggle-light-button-label').text('OFF');
    $('#btn-toggle-light').removeClass('btn-success').addClass('btn-danger');
    $('#lights-mask').removeClass('off').addClass('on');
    boolLightOn = true;
    $('#menu').fadeIn('fast');
};

// This function toggles the LIGHT between ON and OFF, AND it also fires the function to keep track
// of how much power is used...There are MUCH better ways of doing this, but to keep it as simple
// as possible for you to understand, I've done it the long way...
function toggleLights() {
    // IF our "is the light on" varible is true....
    if (boolLightOn === true) {
        // Update the status text, so it should say "The light is currently ON"
        $('#text-light-status').text('OFF');
        $('#toggle-light-button-label').text('ON');
        $('#btn-toggle-light').removeClass('btn-danger').addClass('btn-success');
        $('#lights-mask').removeClass('on').addClass('off');
        // Now, to make sure we can TOGGLE, se the variable to false - ie: TURN THE LIGHT OFF!!
        boolLightOn = false;
    } else {
        // OR, if the light is OFF, we need to switch everything round to the OFF state...assuming we havent reached the toggle limit!

        //If the light has been siwthed on LESS THAN 20 times, then all is good...
        if (numTimesLightUsed < numMaxLightUses) {
            // Increment the toggle usage count by ONE
            $('#text-light-status').text('ON');
            $('#toggle-light-button-label').text('OFF');
            $('#btn-toggle-light').removeClass('btn-success').addClass('btn-danger');
            $('#lights-mask').removeClass('off').addClass('on');
            numTimesLightUsed++;
            // Update the count shown in the UI eg: numTimesLightUsed/numMaxLightUses
            if (numTimesLightUsed === 1) {
                $('#textCountOfTimesLightToggled').text(numTimesLightUsed);
                $('#textCountOfTimesLightToggledinHeading').text(numTimesLightUsed);
                $('#txt-heading-suffix').html('You have toggled the light ONCE');
            } else if (numTimesLightUsed === 2) {
                $('#textCountOfTimesLightToggled').text(numTimesLightUsed);
                $('#textCountOfTimesLightToggledinHeading').text(numTimesLightUsed);
                $('#txt-heading-suffix').html('You have toggled the light TWICE');

            } else {
                $('#textCountOfTimesLightToggled').text(numTimesLightUsed);
                $('#textCountOfTimesLightToggledinHeading').text(numTimesLightUsed);
                $('#txt-heading-suffix').html('You have toggled the light <span id="">' + numTimesLightUsed + '</span> times');
            }
        } else if (numTimesLightUsed === numMaxLightUses) {
            // If we are on the maximum limit or more..then STOP, and show the choices of next actions...RESET, or go back to menu?
            // longer term, this would be where we say "try again" or "GAME OVER" etc..
            var v = document.getElementsByTagName("audio")[0];
            v.play();
            $('#scare').show();
            setTimeout(function() {
                console.log('SHOWING NOW');
                $('#evil-laugh').show().addClass('animated bounceIn');
            }, 2000);
            // alert('OH NOOOOOO, you have used all your light toggles...');

        } else {
            // Henry - This should NEVER FIRE...if you see a box popup that says "OH NOOOOOO - Shouldnt be seeing this EVER!!" something
            // has gone pretty wrong!! ;-) - I'm interest to see if you can explain WHY this is the case....get your thinking hat on!
            //  $('#scare').show();
            alert('OH NOOOOOO - Shouldnt be seeing this EVER!!');

        }
        boolLightOn = true;
    }
}

// This function would show an ALERT BOX to the player/user. It can show several types...
// SUCCESS = a GREEN box
// WARNING = an ORANGE BOX
// DANGER = a RED box
// PRIMARY = a BLUE box
// Think about color usage to show the user the severity or importance of the message you want to
// show..RED == DANGER....GREEN === GOOD etc etc
function showAlert(strAlertType) {

}

// INITIALISE THE CODE WHEN THE WEB PAGE IS FULLY LOADED AND READY TO ROCK!
$(document).ready(function() {

    // ---------- FIRST UP, LETS MAKE SURE THE BUTTONS DO SOMETHING -------
    // ----------------------------- MENU BUTTONS --------------------------
    // This chunk of code deals with the CLICK on the MENU buttons!
    $('body').delegate('li.list-group-item >a', 'click', function(event) {
        var mnuEventName = $(this).attr('href');
        console.log(mnuEventName);
        $('div.screen').each(function() {
            if (!$(this).hasClass('hidden')) {
                $(this).addClass('hidden');
            }
        });
        // FIRE THE MENU FUNCTION TO SHOW THE CORRECT SCREEN!!...
        fireMenu(mnuEventName);

        // STOP ANYTHING ELSE FROM HAPPENING!
        event.preventDefault();
        return false;
    });
    // ----------------------------- END OF MENU BUTTONS 0  ----------------------------------

    // ------------------------------- BACK TO MAIN MENU BUTTON ------------------------------
    $('body').delegate('button.js-backbutton', 'click', function(event) {

        // FIRE THE FUNCTION TO RESET EVERYTHING BACK TO HOW IT WAS BEFORE THE USER DID ANYTHING!
        resetUI();
        // STOP ANYTHING ELSE FROM HAPPENING!
        event.preventDefault();
        return false;
    });
    // -------------------------- END OF BACK TO MAIN MENU BUTTON --------------------------

    //  --------------------------- TOGGLE LIGHT BUTTON ------------------------------------

    $('body').delegate('#btn-toggle-light', 'click', function(event) {
        toggleLights();
    });

    //  ------------------------END TOGGLE LIGHT BUTTON ------------------------------------


    // ------------------------- RESET the UI after the JUMPSCARE --------------------------

    // This demo code has a JUMPSCARE built in...but we need to be able to reset once the player
    // has "browned their trousers"!
    $('body').delegate('#btn-reset-scare', 'click', function(event) {
        $('#evil-laugh').removeClass('animated bounceIn').hide();
        $('#scare').fadeOut();
        numRemainingLightUses = numMaxLightUses;
        numTimesLightUsed = 0;
        $('#lights-mask').removeClass('off').addClass('on');
        $('#textCountOfTimesLightToggled').text(numTimesLightUsed);
        $('#textCountOfTimesLightToggledinHeading').text(numTimesLightUsed);
        $('#txt-heading-suffix').html('You haven\'t toggled the light yet');
        $('#text-light-status').text('ON');
        $('#toggle-light-button-label').text('OFF');
        $('#btn-toggle-light').removeClass('btn-success').addClass('btn-danger');
    });

    // ------------------------- RESET the UI after the JUMPSCARE --------------------------

    //Ok, now we know the buttons are going to do what we need, lets start the other stuff off...

    // Show the current number of times the player has toggled the lights!
    $('#textCountOfTimesLightToggled').text(numTimesLightUsed);
    // Show the MAXIMUM ALLOWED NUMBER OF LIGHT TOGGLES ALLOWED
    $('#textTotalNumberOfLightTogglesAllowed').text(numMaxLightUses);

    // YEAH BABY - We've set everything up...so lets fade the menu in and let the USER/PLAYER 
    // take control....hold onto your hats!
    $('#menu').fadeIn('slow');
});
