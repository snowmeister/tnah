/*

Right then Henry, by now, you should get the idea...VARIABLES where we can tweak settings...
*/
// 1. The RADIUS of the TORCH BEAM..
var beamRadius = 0
    // 2. This is simply a way of reducing the amount of typing we
    // need to do, and makes the spotlight object available to all our functions
    // if you're interested about performance and other deeper code theory, next
    // time we speak, ask me why this way of doing things is a BAD idea :-)
var sp = $('#spotlight');
// We need to keep track of WHERE on the screen the torch spot is at any given time...
var spotpoint = null;
// And we need to be able to access, change and reset etc on the opacities we use to determin how BRIGHT the torch is
var opacity1 = 0.1;
var opacity2 = 1;
// Lets grab the SCREEN width and height
var numScreenWidth = $(window).width();
var numScreenHeight = $(window).height();
// These determine the size the TORCH UI element is rendered at
var numTorchHeight = 50;
var numTorchwidth = 300;
var currentDrain = 0;
var rect1 = null;
var arrHitSpots = [];
// ----------------------------------- VARIABLES ------------------------------------------
// For now, only a FEW variables....but this will increase a LOT as we progress with the game...be ready!! ;)
// How much charge we lose PER TICK of light usage...default value is 1, in other words
// we can have the light on for 100 TICKS...
// Now lets setup the POWER LEVELS STUFF
var currentPowerColor = 'green';
var hitcolor = 'green';
var numTorchChargePercentageLostPerTick = 1;
// Hmm...WTF is a TICK...a TICK as a SET PERIOD OF TIME, a second, or 100 milliseconds, or anything else...it is SET in milliseconds
// For now, a TICK is equivalent to a SECOND (1000 milliseconds!);
var numTickLengthInMilliseconds = 1000;
// We need to be able to set how much power the battery has to begin with...EASY mode is 100%, but we could reduce this for different game difficulty levels! 
var numTotalBatteryChargePercent = 100;
// We need to know HOW Much battery charge we have remaining too!
var numCurrentBatteryChargePercent = numTotalBatteryChargePercent;
// now sure we need this here yet, but its easier for now... 
var currentPercentangeBatteryUsed = 0;
// How many times has the light been used? At the start, NEVER....
var numTimesLightUsed = 0;
var fnTorchInterval = null;
var numTorchIntervalLength = 1000;
// Again, not sure if this is useful or not yet, but
// let's set a limit to HOW MANY TIMES the user can
// switch the light on or off! For now, while we learn,
// I've set it to pick a random number between 3 and 10. Time to experiment to work out the best amount :-)
//
var numMaxLightUses = Math.floor(Math.random() * 10) + 3;
// We need to keep track of how many REMAINING light toggles we have left
var numRemainingLightUses = numMaxLightUses;
// Now we need a way to keep track of the torch runner - this is NULL by default, but will contain a ticking timer keep track of 
// the situation when the player turns the torch on...
var torchRunner = null;
// the torch has varying levels, LOW, Normal, High etc, see arrPowerLevels for more details!...
var torchPower = 200;
// the torch uses a set amount of power per tick....its 1 by default, but again, see arrPowerLevels for more details (powerdrain)
var powerdrain = 1;
// We need to keep track of the torch being on or off, so lets set that up now...
var torchOn = false;
// As agreed, we need a torch with varying power levels and other PROPERTIES. for now, we have a LABEL property, 
// a VALUE property, and a POWERDRAIN property. Each of these VARIABLES will change how the
// torch works, and as result, will affect gameplay!
var arrPowerLevels = [
    // THIS IS THE LOW POWER SETTINGS
    {
        'label': 'Low',
        'value': (numScreenWidth / 100) * 7.5,
        'powerdrain': 0.05
    },
    // THIS IS THE NORMAL POWER SETTINGS
    {
        'label': 'Normal',
        'value': (numScreenWidth / 100) * 15,
        'powerdrain': 0.1
    },
    // THIS IS THE HIGH POWER SETTINGS
    {
        'label': 'High',
        'value': (numScreenWidth / 100) * 20,
        'powerdrain': 0.25
    },
    // THIS IS THE SUPER POWER SETTINGS
    {
        'label': 'SUPER',
        'value': (numScreenWidth / 100) * 30,
        'powerdrain': 0.5
    }
];
var numSelectedPowerLevelIndex = 0;

function moveMaskLayers() {
    var toplayer = sp.getLayer('topfill');
    var bottomlayer = sp.getLayer('bottomfill');
    var leftfill = sp.getLayer('leftfill');
    var rightfill = sp.getLayer('rightfill');
    toplayer.height = spotpoint.y - (beamRadius / 2) - ((beamRadius / 100) * 25),
        bottomlayer.y = spotpoint.y + (beamRadius * 0.75),
        leftfill.width = spotpoint.x - (beamRadius / 2) - ((beamRadius / 100) * 24);
    rightfill.x = spotpoint.x + (beamRadius / 1.2)
    rightfill.width = numScreenWidth,
        sp.drawLayers()
}

function sizeCanvas() {
    sp.attr('width', numScreenWidth);
    sp.attr('height', numScreenHeight);
}


// Props to Adam, http://stackoverflow.com/questions/1484506/random-color-generator-in-javascript - renamed this to make it easier for my son to understand and find functions....
function generateRandomColor(numOfSteps, step) {
    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    var r, g, b;
    var h = step / numOfSteps;
    var i = ~~(h * 6);
    var f = h * 6 - i;
    var q = 1 - f;
    switch (i % 6) {
        case 0:
            r = 1;
            g = f;
            b = 0;
            break;
        case 1:
            r = q;
            g = 1;
            b = 0;
            break;
        case 2:
            r = 0;
            g = 1;
            b = f;
            break;
        case 3:
            r = 0;
            g = q;
            b = 1;
            break;
        case 4:
            r = f;
            g = 0;
            b = 1;
            break;
        case 5:
            r = 1;
            g = 0;
            b = q;
            break;
    }
    var c = "#" + ("00" + (~~(r * 255)).toString(16)).slice(-2) + ("00" + (~~(g * 255)).toString(16)).slice(-2) + ("00" + (~~(b * 255)).toString(16)).slice(-2);
    return (c);
}


function buildFlashlight() {
    var bodyGradient = $('canvas').createGradient({
        // Gradient is drawn relative to layer position
        x1: 0,
        y1: 50,
        x2: 0,
        y2: 100,
        c1: '#ccc',
        c2: '#fff',
        c3: '#666'
    });
    sp.drawRect({
        fillStyle: bodyGradient,
        x: 20,
        y: numScreenHeight - numTorchHeight - 10,
        width: numTorchwidth,
        height: numTorchHeight,
        layer: true,
        cornerRadius: 5,
        groups: ['power-hud'],
        name: 'battery',
        fromCenter: false
    });
    sp.drawRect({
        fillStyle: bodyGradient,
        x: numTorchwidth + 15,
        y: numScreenHeight - numTorchHeight - 10 + ((numTorchHeight / 100) * 20),
        width: (numTorchwidth / 100) * 5,
        height: (numTorchHeight / 100) * 60,
        layer: true,
        cornerRadius: 5,
        groups: ['power-hud'],
        name: 'battery-cap',
        fromCenter: false
    });
    sp.drawRect({
        fillStyle: currentPowerColor,
        x: (numTorchwidth / 100) * 8,
        y: numScreenHeight - numTorchHeight - 10 + ((numTorchHeight / 100) * 10),
        width: (numTorchwidth / 100) * 97,
        height: (numTorchHeight / 100) * 80,
        layer: true,
        cornerRadius: 5,
        groups: ['power-hud'],
        name: 'battery-meter-green',
        fromCenter: false
    });
    sp.drawRect({
        fillStyle: '#ccc',
        x: (numTorchwidth / 100) * 8,
        y: numScreenHeight - numTorchHeight - 10 + ((numTorchHeight / 100) * 10),
        width: (numTorchwidth / 100) * 97,
        height: (numTorchHeight / 100) * 80,
        layer: true,
        cornerRadius: 5,
        groups: ['power-hud'],
        name: 'battery-meter-bg',
        fromCenter: false
    });
    sp.drawText({
        fillStyle: '#fff',
        strokeStyle: '#000',
        strokeWidth: 2,
        x: numTorchwidth + ((numTorchwidth / 100) * 15),
        y: numScreenHeight - ((numTorchHeight / 100) * 125),
        fontSize: 48,
        layer: true,
        fromCenter: false,
        groups: ['power-hud'],
        name: 'battery-percent-text',
        fontFamily: 'Verdana, sans-serif',
        text: numCurrentBatteryChargePercent + '%'
    });
    sp.moveLayer('battery-cap', 21).moveLayer('battery', 20).moveLayer('battery-meter-bg', 22).moveLayer('battery-percent-text', 22).moveLayer('battery-meter-green', 23).drawLayers();
}

function buildPowerSelect() {
    $('#grp-power-level').empty();
    for (var i = 0; i < arrPowerLevels.length; i++) {
        if (i === 0) {
            torchPower = arrPowerLevels[i].value;
        }
        if (numSelectedPowerLevelIndex === i) {
            torchPower = arrPowerLevels[i].value;
            powerdrain = arrPowerLevels[i].powerdrain;
            $('#grp-power-level').append('<label class="radio-inline"><input checked  data-drainage="' + arrPowerLevels[i].powerdrain + '" type="radio" name="power-level" id="inlineRadio1" value="' + arrPowerLevels[i].value + '" > ' + arrPowerLevels[i].label + ' </label>');
        } else {
            $('#grp-power-level').append('<label class="radio-inline"><input  data-drainage="' + arrPowerLevels[i].powerdrain + '" type="radio" name="power-level" id="inlineRadio1" value="' + arrPowerLevels[i].value + '" > ' + arrPowerLevels[i].label + ' </label>');
        }
    }
    beamRadius = $('input[name="power-level"]:checked').val();
    currentDrain = $('input[name="power-level"]:checked').data('drainage');
}

function detect(rect1) {
    // console.clear();
    var collide = 0;

    //console.log('WE ARE DETECTING');
    //console.dir(arrHitSpots);
    for(var i = 0; i < arrHitSpots.length ; i ++){
        var rect2 = arrHitSpots[i];
        if (rect1.x < rect2.x + rect2.width && rect1.x + (rect1.width + (beamRadius/2)) > rect2.x && rect1.y < rect2.y + (rect2.height - (beamRadius/2)) && rect1.height + rect1.y - (beamRadius/4)> rect2.y) {
            collide ++
        }else{
            collide--;
        }
    }
    if(collide === 0){
        hitcolor = 'green'
    }else{
        hitcolor = 'red'
    }
}


function showBounds(layer) {
    sp.drawRect({
        strokeStyle: hitcolor,
        strokeWidth: 4,
        x: spotpoint.x,
        y: spotpoint.y,
        width: beamRadius * 1.25,
        height: beamRadius * 1.25,
        fromCenter: true,
        cornerRadius: 5,
        name: 'detectBounds'
    });
    rect1 = { 'y': Math.round(spotpoint.y), 'x': Math.round(spotpoint.x), 'height': beamRadius * 1.25, 'width': beamRadius * 1.25 };
    detect(rect1);
}


function buildBeam() {
    sp.removeLayer('spot').removeLayer('rightfill').removeLayer('leftfill').removeLayer('topfill').removeLayer('bottomfill').drawLayers();
    buildFlashlight();
    sp.drawArc({
        fillStyle: function(layer) {
            return $(this).createGradient({
                // Gradient is drawn relative to layer position
                x1: 0 + layer.x,
                y1: 0 + layer.y,
                x2: 0 + layer.x,
                y2: 0 + layer.y,
                r1: 0,
                r2: beamRadius * 1.5,
                c1: 'rgba(0, 0, 0, ' + opacity1 + ')',
                c2: 'rgba(0, 0, 0, ' + opacity1 + ')',
                c3: 'rgba(0, 0, 0, ' + opacity2 + ')',
                c4: 'rgba(0, 0, 0, ' + opacity2 + ')',
                c5: 'rgba(0, 0, 0, ' + opacity2 + ')',
            });
        },
        x: (numScreenWidth / 2) - ((beamRadius / 2) - (beamRadius / 2)),
        y: (numScreenHeight / 2) - ((beamRadius / 2) - (beamRadius / 2)),
        scale: 1,
        radius: beamRadius,
        strokeStyle: 'rgba(0, 0, 0, ' + opacity2 + ')',
        strokeWidth: beamRadius / 2,
        fromCenter: true,
        layer: true,
        name: 'spot',
        draggable: true,
        drag: function(layer) {
            moveMaskLayers();

            //console.log(layer.x - (beamRadius/2) );
            showBounds(layer);
        }
    });
    spotpoint = sp.getLayer('spot');
    sp.drawRect({
        fillStyle: 'black',
        x: 0,
        y: 0,
        width: numScreenWidth,
        fromCenter: false,
        height: spotpoint.y - (beamRadius / 2) - ((beamRadius / 100) * 25),
        layer: true,
        name: 'topfill'
    }).drawRect({
        fillStyle: 'black',
        x: 0,
        y: 0,
        width: spotpoint.x - (beamRadius / 2) - ((beamRadius / 100) * 24),
        fromCenter: false,
        height: (numScreenHeight * 2),
        layer: true,
        name: 'leftfill'
    }).drawRect({
        fillStyle: 'black',
        x: spotpoint.x + (beamRadius / 1.2),
        y: 0,
        width: numScreenWidth,
        fromCenter: false,
        height: (numScreenHeight * 2),
        width: 2000,
        layer: true,
        name: 'rightfill',
    }).drawRect({
        fillStyle: 'black',
        x: 0,
        y: spotpoint.y + (beamRadius * 0.75),
        width: numScreenWidth,
        fromCenter: false,
        height: numScreenHeight,
        layer: true,
        name: 'bottomfill',
        groups: ['spotlight'],
    }).drawLayers();
    sp.moveLayer('battery-cap', 21).moveLayer('battery', 20).moveLayer('battery-meter-bg', 22).moveLayer('battery-percent-text', 22).moveLayer('battery-meter-green', 23).drawLayers();
}

function scaleAndColorBattery() {
    currentPowerColor = 'green';
    if ((numCurrentBatteryChargePercent > 33) && (numCurrentBatteryChargePercent < 66)) {
        currentPowerColor = 'orange'
        opacity1 = 0.25
    }
    if (numCurrentBatteryChargePercent <= 33) {
        opacity1 = 0.5
        currentPowerColor = 'red'
    }
    var progressWidth = sp.getLayer('battery-meter-green');
    var w = (numTorchwidth / 100) * 97;
    var removeWidth = ((w / 100) * numCurrentBatteryChargePercent);
    var newW = removeWidth;
    if (torchOn === true) {
        var progressbar = sp.setLayer('battery-meter-green', {
            width: newW,
            fillStyle: currentPowerColor
        }).drawLayers();
        var percText = sp.setLayer('battery-percent-text', {
            text: Math.floor(numCurrentBatteryChargePercent) + '%'
        });
    }
}

function useBattery() {
    if (torchOn === true) {
        numCurrentBatteryChargePercent -= currentDrain;
        scaleAndColorBattery();
    }
    if (numCurrentBatteryChargePercent < 0) {
        numCurrentBatteryChargePercent = 0;
        $('#spotlight').toggleClass('hide');
        scaleAndColorBattery();
        var percText = sp.setLayer('battery-percent-text', {
            text: '0%'
        });
        var progressbar = sp.setLayer('battery-meter-green', {
            width: 0,
            fillStyle: 'red'

        }).drawLayers();
        $('#btn-toggle-torch').toggleClass('btn-danger btn-success');
        scaleAndColorBattery();
        torchOn = false;
        clearInterval(fnTorchInterval);
        fnTorchInterval = null;
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function setupHitSpots() {
    arrHitSpots = []
    $('div.hitspot').each(function() {
        var randomWidth = (numScreenWidth / 100) * getRandomInt(20, 35);
        var randomHeight = (numScreenHeight / 100) * getRandomInt(20, 35);
        var topPoint = ((numScreenHeight / 100) * getRandomInt(1, 100)) - randomHeight;
        var leftPoint = ((numScreenWidth / 100) * getRandomInt(1, 100)) - randomWidth;
        $(this).css({
            'backgroundColor': generateRandomColor(getRandomInt(100, 200), getRandomInt(101, 199)),
            'width': randomWidth,
            'height': randomHeight,
            'top': topPoint,
            'left': leftPoint
        });
        arrHitSpots.push({ 'y': Math.round(topPoint), 'x': Math.round(leftPoint), 'height': randomHeight, 'width': randomWidth });
    });
}
$(document).ready(function() {
    beamRadius = $('input[name="power-level"]:checked').val();
    $('body').delegate('input[name="power-level"]', 'change', function(event) {
        beamRadius = $(this).val();
        sizeCanvas();
        buildBeam();
        currentDrain = $(this).data('drainage');
        if (torchOn === true) {
            fnTorchInterval = setInterval(function() {
                useBattery();
            }, numTickLengthInMilliseconds);
        } else {
            fnTorchInterval = null;
        }
    });
    $('#btn-toggle-torch').on('click', function(event) {
        if (torchOn === true) {
            torchOn = false;
            currentDrain = 0;
            fnTorchInterval = null;
        } else {
            torchOn = true;
            fnTorchInterval = setInterval(function() {
                useBattery();
            }, numTickLengthInMilliseconds);
        }
        $(this).toggleClass('btn-danger btn-success');
        $('#spotlight').toggleClass('hide');
        event.preventDefault();
        return false;
    });
    buildPowerSelect();
    sizeCanvas();
    buildBeam();
    buildFlashlight();
    setupHitSpots();
});
