/*

 Right. This is the VERY EARLY R&D for our JSON based "GAME ENGINE" that will controll WHICH SCREENS are shown, WHEN they 
 are shown, what ACTIONS are available etc. At present, this is entirely driven with STATIC DUMMY DATA, and rendered based on
 CLIENT-SIDE code. The production version will be more SERVER-SIDE based and use DYNAMIC DATA

 */

// Lets try to be MODULAR....and we're gonna need jQuery ($)...
var TNAH = (function ($) {
	 _.templateSettings.variable = "level";
	var currentLevel = 0;
	var leveldata = [];
	var currentData = [];
	// Lets grab ME (this), for future usage...ask me about this Henry!...we EXPORT this...
	var _me = this;
	// Thi is just a SHORTCUT, saves on typing!
	var b = $('body');
	// This function is a DRY thing, it builds LIST ITEMS, DRY = Dont Repeat Yourself..this is REUSABLE code that we will us anywhere 
	// with ORDERED LISTS, UNORDERED LISTS, and SELECTS, but could also be used for other elements too... 
	_me.buildListItem = function(tag, str, attr, value, showicon, icon, itemclass){
		var iconClass ='';
		var iconHTML ='';
		if(showicon === true){
			iconClass = 'fa ' + icon;
			iconHTML = '<i class="'+iconClass+'"></i>'; 
		}
		var li = '<'+tag+' class="'+itemclass+'" '+attr+ '="'+value+ '"> ' + str +' '+ iconHTML + '</'+tag+'>';
		return li;
	}
	// This function returns the HTML for a LIST element, and can return UL or OLs, set custom classnames, id, data to populate items, display style and the CSS classnames to be used on CHILD itams
	_me.buildList = function(listtype,stylename, id, data, block, itemclass){
		if(block === true){
			stylename = stylename + ' col-xs-12' ;
		}
		var list = '<'+listtype+' class="'+stylename+'" id="'+id+'">';
		for (var i = 0 ; i < data.length ; i ++){
			list += _me.buildListItem('li', data[i].name, 'rel',  data[i].id, true, 'fa-eye pull-right js-show-level-data', itemclass);
		}
		list += '</'+ listtype +'>';
		return list;
	}

	_me.showCharacters = function(){

	};
	/* This code will ONLY fire first time out...it is called by doIntro! */
	_me.doLevelsList = function(data){
		// lets bang the level data into the TNAH level array variable for use elsewhere..
		leveldata = data.gamedata.levels;
		// Not idea, we should try to AVOID having HTML in JavaScript - ASK ME about Separation of Concerns...
		var html = '<p>The list below shows the DUMMY DATA we have for LEVELS, and the fact that you are ';
		html += 'SEEING this section of text illustrates how we can use DATA to decide WHICH CODE to run...Henry, ';
		html += 'ANOTHER ONE TO ASK ME ABOUT, but for now, suffice to say, this is FUNDAMENTAL to our Game Engine</p>';
		html += _me.buildList('ul', 'list-unstyled list-group ','levels-list', data.gamedata.levels, true, 'list-group-item list-group-item-info list-levels clearfix');
		// APPEND the html for the list to the panel body..
		$('#main-panel-body').append(html);
	}
	/* 
	This function will ONLY fire IF the GAME DATA file is successfully loaded!! and if currentLevel is 0 (ZERO) - In other words, the FIRST TIME the
	GAME ENGINE RUNNS. This is determined by the CALLBACK property in the JSON dataset for the FIRST LEVEL...This function is ONLY for our RnD
	 */
	_me.doIntro = function(data){
		$('#main-panel-title').html(data.game.title);
		$('#main-panel-body').html('<p>' + data.game.description + '</p>');
		$('#main-panel').removeClass('hidden');
		_me[data.gamedata.levels[currentLevel].callback](data);
	};
	// This is the main function that loads our GAME DATA
	_me.loadGameData = function(data_id, callback){
		// Right, XHR is our XmlHttpRequest - Henry ASK ME about this!! It's how we LOAD the data into our code so 
		// we can run our game!
		var xhr = $.ajax({
			type: 'get',
			dataType:'json',
			url: 'mocks/data/engine/'+data_id+'.json',
			success: function(data){
				currentData = data;
				callback(data);
			},
			 error: function (request, status, error) {
        		alert(request.responseText);
    		}
		});
	}
	// This is an R&D only function, used to build up the page sections to display our dummy game data...
	_me.buildDetailsPanes = function(id){
		var details =  _.find(leveldata, function (o) { return o.id == id; });
		var template = _.template(
            $( "script.js-details-template" ).html()
        );
		$( "#details-region" ).html(template( details ));
		console.dir(details);
	}
	// This is a SELF CALLING function, it will run when this MODULE is INSTANTIATED - Again Henry, ASK ME about this...
	_me.init = (function(){
		// Let's load the data up to set things in motion!
		_me.loadGameData(currentLevel, _me.doIntro);
		// This handles the CLICK on the EYE icons for the LEVELS list...
		b.delegate('i.js-show-level-data','click', function(event){
		});
		// This controls the LEVELS list on the DMEO home screen
		b.delegate('li.list-levels', 'click', function(event){
			// User Experience - Lets make sure ONLY the CURRENTLY selected item is HIGHLIGHTED in the UI!...first, DESELECT all the items with the CSS class "active"...
			$('li.active').removeClass('active');
			// Now lets add the "active" class to the LATEST (CURRENT) selected element...
			$(this).addClass('active');
			// Let's make it easier for ourselves again by reducting typing....
			var el = $(this);
			var id = el.attr('rel');
			// remove the EXISTING details region, IF it existins....
			$('#details-region').remove();
			// Noe, lets WRITE a NEW DETAILS REGION....
			el.append('<div id="details-region" class="push-down-10 sub-panel clearfix">DATA</div>');
			_me.buildDetailsPanes(id);
		});
	})();
	return _me;
}(jQuery));